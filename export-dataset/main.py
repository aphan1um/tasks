import psycopg2
import os
import minio
import pytz
from datetime import datetime

bucket_name = os.environ['MINIO_BUCKET_NAME']

print(f"[INFO] Connecting to Postgres... ({os.environ['PG_HOST']}:{os.environ['PG_PORT']})")
sql_conn = psycopg2.connect(
  host = os.environ['PG_HOST'],
  port = os.environ['PG_PORT'],
  dbname = os.environ['PG_DBNAME'],
  user = os.environ['PG_USERNAME'],
  password = os.environ['PG_PASSWORD'],
)
print('[INFO] Connected to Postgres!')

print(f"[INFO] Connecting to Minio... ({os.environ['MINIO_ENDPOINT']})")
minio_client = minio.Minio(
  os.environ['MINIO_ENDPOINT'],
  access_key = os.environ['MINIO_ACCESS_KEY'],
  secret_key = os.environ['MINIO_SECRET_KEY'],
  secure = False
)
print('[INFO] Connected to Minio!')

cur = sql_conn.cursor()


if not minio_client.bucket_exists(bucket_name):
  minio_client.make_bucket(bucket_name)

print(f"[INFO] Bucket '{bucket_name}' has been made or already existed")

print('[INFO] Refreshing materialised dataset')
cur.execute('REFRESH MATERIALIZED VIEW viewdataset');

print('[INFO] Exporting dataset as CSV')
with open('result', 'w') as f:
  cur.copy_expert('COPY (SELECT * FROM viewdataset) TO STDOUT WITH CSV HEADER', f)

print('[INFO] Putting exported CSV into S3 bucket...')
minio_client.fput_object(
  bucket_name,
  f"[{datetime.now(pytz.timezone('Australia/Victoria')).strftime('%m-%d-%Y %H:%M:%S')}] viewdataset.csv",
  'result',
  content_type='text/csv'
)
